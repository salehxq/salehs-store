<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Http\Requests\StoreStoreRequest;
use App\Http\Requests\UpdateStoreRequest;
use Illuminate\Http\Request;


class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('stores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStoreRequest $request)
    {
        $validateData = $request->validate([
            'name' => 'required|min:5',
            'description' => 'required|min:5',
            'logo' => 'mimes:jpeg,bmp,png,jpg|max:3000',
        ]);
        $path = null;
        
        if($request->hasFile('logo'))
            $path = '/storage/' .$request->file('logo')->store('logos',['disk'=>'public']);

        $newStore = new Store();
        $newStore->name = $request->name;
        $newStore->description = $request->description;
        $newStore->image=$path;
        
        auth()->user()->stores()->save($newStore);

        return redirect('users/account');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        return view('stores.show',compact('store'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        if($store->user_id != auth()->user()->id)
            return 'لست صاحب المتجر';
        return view('stores.edit',compact('store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateStoreRequest  $request
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStoreRequest $request, Store $store)
    {
        $validateData = $request->validate([
            'name' => 'required|min:5',
            'description' => 'required|min:5',
            'logo' => 'mimes:jpeg,bmp,png,jpg|max:3000',
        ]);
        
        $path = $store->image;

        if($request->hasFile('logo'))
        $path = '/storage/' .$request->file('logo')->store('logos',['disk'=>'public']);

        $store->name=$request->name;
        $store->description=$request->description;
        $store->image=$path;
        $store->save();

        return redirect('users/account');
    }
    public function products(Request $request, Store $store){
            if($store->user_id != auth()->user()->id)
                return view('stores.error');
            return view('stores.products',compact('store'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        if($store->user_id != auth()->user()->id)
        return view('stores.error'); 
        $store->delete();
        return redirect('users/account');
    }

    public function orders(){
        $stores = auth()->user()->stores;

        $orders = array();
        foreach($stores as $store){
            foreach($store->orders as $order){
                $orders[] = $order;
            }
        }
        return view('stores.orders',compact('orders'));

    }

}
