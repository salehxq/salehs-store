<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

class UsersController extends Controller
{
    public function account(){
        return view('users.account');
    }

    public function logout(){
        auth()->logout();
        session()->flush();
        return redirect('/');
    }

    public function secure(){
        return view('secure');
    }

    public function login(){
        if(auth()->check()){
            return redirect('/');
        }
        return view('users.login');
    }

    public function tryLogin(){
        $validatedData = request()->validate([
            'email' => 'required',
            'password' => 'required|min:5'
    ]);

        if( auth()->attempt(['email'=>request()->email,'password'=>request()->password])){
            return redirect()->intended('/');
        }else{
            return back()->withErrors(['معلومات خاطئة']);
        }
    }
    
    public function create(){
        return view('users.register');
    }
    
    public function store(){
        $validatedData = request()->validate([
            'name' =>'required|min:3',
            'number' => 'required|min:8|unique:users',
            'email'=>'required|unique:users',
            'password' => 'required|min:5',
            'confirm' => 'required|same:password'
        ]);

    $user = new User();

    $user->name=request()->name;
    
    $user->number=request()->number;

    $user->email=request()->email;

    $user->password=bcrypt(request()->password);

    $user->email_verified_at = now();

    $user->save();

    if( auth()->attempt(['email'=>request()->email,'password'=>request()->password])){
        return redirect('/');
    }
}

    public function address(){
        $address = auth()->user()->address;        
        return view('users.address',compact('address'));
    }

    public function storeAddress(){
        $validatedData = request()->validate([
            'area' => 'required',
            'block' => 'required',
            'street' => 'required',
            'house' => 'required',
        ]);

        $newArray=[
            'area'=>request()->area,
            'block'=>request()->block,
            'street'=>request()->street,
            'house'=>request()->house,
            'extra'=>request()->extra,
        ];
        auth()->user()->address=$newArray;
        auth()->user()->save();
        return redirect('users/address');
    }

    public function edit(){
        $user = auth()->user();
        return view('users.edit',compact('user'));
    }
    
    public function update(){
        
        $validatedData = request()->validate([
            'name' =>'required|min:3',
            'number' => 'required|min:8|unique:users,id,'.auth()->user()->id,
            'email'=>'required|unique:users,id,'.auth()->user()->id,
        ]);

    $user = auth()->user();

    $user->name=request()->name;
    
    $user->number=request()->number;

    $user->email=request()->email;

    $user->password=bcrypt(request()->password);

    $user->email_verified_at = now();

    $user->save();

    return redirect('/users/edit');
    }
}
