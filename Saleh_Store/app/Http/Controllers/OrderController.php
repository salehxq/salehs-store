<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Repositories\OrderRepository;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderReceivedTwo;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isset(auth()->user()->orders)){
        $orders = auth()->user()->orders;
        return view('orders.index',compact('orders'));
        }else{
            return view('users.login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!session()->has('currentOrders'))
        return redirect('/');

        $total = 0;

        foreach(session('currentOrders') as $product){
        $total+=$product->price*$product->quantity;
        }

        return view('orders.create',compact('total'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOrderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrderRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $total = 0;
        foreach ($order->products as $product){
            $total += $product['quantity']*$product['price'];
        }
        return view('orders.show',compact('order','total'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOrderRequest  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrderRequest $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
    
public function addProduct(Product $product){
        $validateData = request()->validate([
            'quantity' => 'required|integer|min:1',
        ]);

        $currentOrders = session()->has('currentOrders') ? session('currentOrders') : array();

        if(!empty($currentOrders)){
            if($currentOrders[0]->store_id != $product->store_id)
            $currentOrders=array();
        }

        $alreadyExist = false;
        foreach($currentOrders as $order){
            if($order->id == $product->id){
                $alreadyExist=true;
                $order->quantity += request()->quantity;
            }
        }
    if(!$alreadyExist){
        $product->quantity = request()->quantity;
        $currentOrders[] = $product;
    }
    session(['currentOrders' => $currentOrders]);
    

    return back()->with('messages',[' تمت اضافة السلعة '.$product->name.' الى طلب بكمية '.request()->quantity.' بنجاح ']);
    }

    public function deliverd(Order $order){
        if($order->store->user->id != auth()->user()->id)
            return 'هذا المتجر ليس ملكك';
        if(!in_array($order->status, ['paid','pending']))
          return 'لا يمكنك تغيير حالة الطلب';
          
          
        $order->status="delivered";
        $order->save();
        return back();
    }
}
