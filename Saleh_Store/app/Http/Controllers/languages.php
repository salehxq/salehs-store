<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class languages extends Controller
{
    public function ar(){
        session(['lang'=>'ar']);
        return back();
    }
    public function en(){
        session(['lang'=>'en']);
        return back();
    }
}
