<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','App\Http\Controllers\HomeController@index');

Route::get('/login',function(){
   return redirect('/users/login');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::prefix('stores')->group(function(){
    Route::group(['middleware' =>['web','auth']],function(){
    Route::get('/orders','App\Http\Controllers\StoreController@orders');
    Route::get('/create','App\Http\Controllers\StoreController@create');
    Route::post('/store','App\Http\Controllers\StoreController@store');
    Route::get('/{store}/edit','App\Http\Controllers\StoreController@edit');
    Route::put('/{store}','App\Http\Controllers\StoreController@update');
    Route::get('/{store}/products','App\Http\Controllers\StoreController@products');
    Route::get('/{store}/delete','App\Http\Controllers\StoreController@destroy');
    });
    Route::get('/{store}','App\Http\Controllers\StoreController@show');
});

Route::prefix('users')->group(function(){
Route::get('/login','App\Http\Controllers\UsersController@login')->name('login');

Route::get('/create','App\Http\Controllers\UsersController@create');

Route::post('/login','App\Http\Controllers\UsersController@tryLogin');

Route::post('/store','App\Http\Controllers\UsersController@store');

Route::group(['middleware' =>['web','auth']],function(){

Route::get('/secure','App\Http\Controllers\UsersController@secure')->middleware('web','auth');

Route::get('/account','App\Http\Controllers\UsersController@account');

Route::get('/logout','App\Http\Controllers\UsersController@logout');

Route::get('/address', 'App\Http\Controllers\UsersController@address');

Route::post('/address', 'App\Http\Controllers\UsersController@storeAddress');


Route::get('/edit','App\Http\Controllers\UsersController@edit');

Route::post('/edit','App\Http\Controllers\UsersController@update');


});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


});

Route::prefix('products')->group(function(){
    Route::group(['middleware' => ['web','auth']],function(){
    Route::get('/create/{store}','App\Http\Controllers\ProductController@create');
    Route::post('/{store}','App\Http\Controllers\ProductController@store');
    Route::get('/{product}/edit ','App\Http\Controllers\ProductController@edit');
    Route::put('/{product}','App\Http\Controllers\ProductController@update');
    Route::get('/{product}/delete','App\Http\Controllers\ProductController@destroy');
    }); 
    Route::get('/{product}','App\Http\Controllers\ProductController@show');
});

Route::group(['prefix'=>'orders','middleware'=>'web','auth'],function(){

Route::get('/','App\Http\Controllers\OrderController@index');

Route::get('/demo',function(){
    return view('orders/demo');
});

Route::get('/create','App\Http\Controllers\OrderController@create');

Route::get('/chargeRequest','App\Http\Controllers\OrderController@chargeRequest');

Route::get('/chargeUpdate','App\Http\Controllers\OrderController@chargeUpdate');

Route::post('/addProduct/{product}','App\Http\Controllers\OrderController@addProduct');

Route::get('/{order}','App\Http\Controllers\OrderController@show');

Route::get('/{order}/deliverd','App\Http\Controllers\OrderController@deliverd');
});
Route::get('/ar','App\Http\Controllers\languages@ar');
Route::get('/en','App\Http\Controllers\languages@en');
Auth::routes();