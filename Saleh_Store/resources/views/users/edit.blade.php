@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-4">
    @component('shared.accountsList',['edit'=>'active'])
    @endcomponent
    </div>
    <div class="col-4">
    @include('shared.errors')
    <form method="post" action="/users/edit">
    @csrf
    <div class="mb-3">
        <label for="name" class="form-label">الأسم</label>
        <input type="text" name="name" class="form-control" id="exampleInputtext1" value='{{$user->name}}' aria-describedby="textHelp" placeholder="الأسم">
    </div>
    <div class="mb-3">
        <label for="email" class="form-label">البريد الإلكتروني</label>
        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value='{{$user->email}}' placeholder="البريد الإلكتروني">
        <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
    </div>
    <div class="mb-3">
        <label for="number" class="form-label">رقم الهاتف</label>
        <input type="number" name="number" class="form-control" id="exampleInputtext1" aria-describedby="textHelp" value='{{$user->number}}' placeholder="رقم الهاتف">
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">كلمة المرور</label>
        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="كلمة المرور">
    </div>
    <div class="mb-3">
        <label for="confirm" class="form-label">تأكيد كلمة المرور</label>
        <input type="password" name="confirm" class="form-control" id="exampleInputPassword1"  placeholder="تأكيد كلمة المرور">
    </div>
    <button type="submit" class="btn btn-dark">تعديل</button>
    </form>
</div>
@endsection
    </div>
</div>


