@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-4">
    @component('shared.accountsList',['address'=>'active'])
    @endcomponent
    </div>
    <div class="col-4">
    @include('shared.errors')
    <form method="post" action="/users/address">
    @csrf
    <div class="mb-3">
        <label for="area" class="form-label">المنطقة</label>
        <input type="text" name="area" class="form-control" value="{{ empty($address ['area']) ? null  : $address ['area']}}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="أسم المنطقة" required>
    </div>
    <div class="mb-3">
        <label for="block" class="form-label">القطعة</label>
        <input type="text" name="block" class="form-control" value="{{ empty($address ['block']) ? null : $address ['block']}}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="أسم القطعة" required>
    </div>

    <div class="mb-3">
        <label for="street" class="form-label">الشارع</label>
        <input type="text" name="street" class="form-control" value="{{ empty($address ['street']) ? null  : $address ['street']}}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="أسم الشارع" required>
    </div>

    <div class="mb-3">
        <label for="house" class="form-label">المنزل</label>
        <input type="text" name="house" class="form-control" value="{{ empty($address ['house']) ? null  : $address ['house']}}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="أسم المنزل" required>
    </div>

    <div class="mb-3">
        <label for="extra" class="form-label">معلومات إضافية</label>
        <input type="text" name="extra" class="form-control" value="{{ empty($address ['extra']) ? null : $address ['extra']}}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="معلومات إضافية(أختياري)">
    </div>
    <button type="submit" class="btn btn-dark">حفظ</button>
    </form>
</div>
@endsection
    </div>
</div>
