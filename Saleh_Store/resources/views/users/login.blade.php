@extends('layouts.app')

@section('content')
<div class="mx-auto w-25 p-3">
    @include('shared.errors')
    <form method="post" action="/users/login">
    @csrf
    <div class="mb-3">
        <label for="email" class="form-label">البريد الإلكتروني</label>
        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="البريد الإلكتروني" required>
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">كلمة المرور</label>
        <input type="password" name="password" class="form-control" id="exampleInputPassword1"  placeholder="كلمة المرور" required>
    </div>
    <button type="submit" class="btn btn-dark">تسجيل الدخول</button>
    <a href="create">انشاء حساب جديد</a>
    </form>
</div>
@endsection