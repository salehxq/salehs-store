@extends('layouts.app');

@section('content')

@section('styles')
<style>
    .card{
        min-width:200px;
        max-width:200px;
        margin: 2% !important;
    }
    .card img{
        height:150px;
        object-fit:cover;
    }
</style>
@endsection
<div class="row">
 
<div class="col-4">
    @component('shared.accountsList',['stores'=>'active'])
    @endcomponent
</div>

<div class="col-8">
        <div class="card-group">
            @foreach(auth()->user()->stores as $store)
                <div class="card" style="width: 60%;">
            
                @isset($store->image)
                    <img src="{{ asset($store->image) }}" class="card-img-top" alt="...">
                @endisset

                @empty($store->image)
                    <img src="{{ asset('store.jpg') }}" class="card-img-top" alt="...">
                @endempty
                <div class="card-body">   
                    <h5 class="card-title">{{ $store -> name }}</h5>
                    <p class="card-text">{{ $store -> description }}</p>
            <center>
                    <a href="/stores/{{ $store -> id }}/edit" class="btn btn-dark">تعديل المتجر</a>
                    <br><br>
                    <a href="/stores/{{ $store -> id }}/products" class="btn btn-dark">تعديل السلع</a>
                    <br><br>
                    <a href="/stores/{{ $store -> id }}/delete" class="btn btn-danger">مسح المتجر</a>
            </center>
                </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
</div>
@endsection