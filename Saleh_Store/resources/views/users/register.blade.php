@extends('layouts.app')

@section('content')
<div class="mx-auto w-25 p-3">
    @include('shared.errors')
    <form method="post" action="/users/store">
    @csrf
    <div class="mb-3">
        <label for="name" class="form-label">الأسم</label>
        <input type="text" name="name" class="form-control" id="exampleInputtext1" aria-describedby="textHelp" value="{{ old('name') }}" placeholder="الأسم">
    </div>
    <div class="mb-3">
        <label for="email" class="form-label">البريد الإلكتروني</label>
        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ old('email') }}" placeholder="البريد الإلكتروني">
        <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
    </div>
    <div class="mb-3">
        <label for="number" class="form-label">رقم الهاتف</label>
        <input type="number" name="number" class="form-control" id="exampleInputtext1" aria-describedby="textHelp" value="{{ old('number') }}" placeholder="رقم الهاتف">
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">كلمة المرور</label>
        <input type="password" name="password" class="form-control" id="exampleInputPassword1" value="{{ old('password') }}" placeholder="كلمة المرور">
    </div>
    <div class="mb-3">
        <label for="confirm" class="form-label">تأكيد كلمة المرور</label>
        <input type="password" name="confirm" class="form-control" id="exampleInputPassword1" value="{{ old('confirm') }}" placeholder="تأكيد كلمة المرور">
    </div>
    <button type="submit" class="btn btn-dark">انشاء حساب جديد</button>
    </form>
</div>
@endsection