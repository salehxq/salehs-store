@extends('layouts.app')

@section('content')

@section('styles')
<style>
    .card{
        min-width:200px;
        max-width:200px;
        margin: 2% !important;
    }
</style>
@endsection
@include('layouts.carousel')
<center>
<h4 style="color:#700414">@lang('messages.warning')</h4>
</center>
<br>
@auth
<h3>@lang('messages.hello')،<br> {{auth()->user()->name}}</h3>
@endauth
<center>
<div class="row">

    <div class="col-12">
            <div class="card-group">
                    @foreach($stores as $store)
                        <div class="card rounded" style="width: 60%">
                    
                        @isset($store->image)
                            <img src="{{ asset($store->image) }}" class="card-img-top" alt="...">
                        @endisset

                        @empty($store->image)
                            <img src="{{ asset('programmer1.gif') }}" class="card-img-top" alt="...">
                        @endempty
                        <div class="card-body">   
                            <h5 class="card-title">@lang('messages.storename') <br>{{ $store -> name }}</h5>
                            &nbsp;
                    <center>
                            <a href="/stores/{{ $store -> id }}" style="width:100%;background-color:#FCA310" class="btn">@lang('messages.storevisit')</a>
                    </center>
                        </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</center>
@endsection
