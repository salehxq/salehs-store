<center>
@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
                <table class="table">
            <thead>
                <tr>
                <th scope="col">السلعة</th>
                <th scope="col">السعر</th>
                <th scope="col">الكمية</th>
                </tr>
            </thead>
            <tbody>
            @foreach($order->products as $product)
            <tr>
                <td style="margin-right:5px">{{ $product['name'] }}</td>
                <td style="margin-right:5px">{{ $product['price']. "درهم" }}</td>
                <td style="margin-right:5px">{{ $product['quantity'] }}</td>
            </tr>
            @endforeach
            <tr>
            </tr>
            </table>
        </tbody>
    </div>
</div>
@endsection
</center>