@component('mail::message')
# استلام الطلب

لقد استلمت طلب جديد

@component('mail::button', ['url' => 'http://localhost:800/orders'])
Button Text
@endcomponent

شكراً,<br>
{{ config('app.name') }}
@endcomponent
