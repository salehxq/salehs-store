<div class="list-group">
<a href="/home" class="list-group-item list-group-item-action {{ $allstores ?? '' }}" aria-current="true">@lang('messages.Visit')</a>
    <a href="/users/account" class="list-group-item list-group-item-action {{ $stores  ?? '' }}" aria-current="true">@lang('messages.Shops')</a>
    <a href="/orders" class="list-group-item list-group-item-action {{ $orders  ?? '' }}" aria-current="true">@lang('messages.Orders')</a>
    @isset(auth()->user)
    @if((sizeof(auth()->user()->stores) > 0))
    <a href="/stores/orders" class="list-group-item list-group-item-action {{ $StoreOrders  ?? '' }} " aria-current="true">@lang('messages.StoreOrders')</a>
    @endif
    @endisset
    <a href="/stores/create" class="list-group-item list-group-item-action {{ $createstore  ?? '' }}">@lang('messages.Create')</a>
    <a href="/users/address" class="list-group-item list-group-item-action {{ $address  ?? '' }}">@lang('messages.Address')</a>
    <a href="/users/edit" class="list-group-item list-group-item-action {{ $edit  ?? '' }}">@lang('messages.personal')</a>
</div>