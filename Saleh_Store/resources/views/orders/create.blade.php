@extends('layouts.app')

@section('content')
<style>
    .color:hover{
        box-shadow: 10px 10px 0px #BFC8EA ;
        top: -5px;
        left: -5px;
        border-radius: 0%;
    }
    .color{
        background:#7B86AA;
    }
    .color2:hover{
        box-shadow: 10px 10px 0px #7B86AA ;
        top: -5px;
        left: -5px;
        border-radius: 0%;
    }
    .color2{
        background:#BFC8EA
    }
</style>
<div class="row">
    <div class="col-12">
                <table class="table">
            <thead>
                <tr>
                <th scope="col">السلعة</th>
                <th scope="col">السعر</th>
                <th scope="col">الكمية</th>
                </tr>
            </thead>
            <tbody>
                @foreach(session('currentOrders') as $product)
            <tr>
                <td>{{ $product -> name }}</td>
                <td>{{ $product -> price }}</td>
                <td>{{ $product -> quantity }}</td>
            </tr>
            @endforeach
            <tr>
                <td>
                    المجموع : {{ $total }} درهم
                </td>
            </tr>
            </table>
            <a href="/" style="width:100%;" class="btn color">أكمل التسوق</a>
            <br><br>
            <a href="/orders/demo" style="width:100%;" class="color2 mr-2 btn {{ empty(auth()->user()->address) ? 'disabled' : '' }}">إتمام عملية الدفع&#160&#160<i class="fas fa-credit-card "></i></a>
            @empty(auth()->user()->address)
            يرجى منك تعبئة بيانات <a href="/users/address">العنوان</a>
            @endempty
            </tbody>

    </div>
</div>

@endsection