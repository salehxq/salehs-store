@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-4">
        @component('shared.accountsList',['orders'=>'active'])
        @endcomponent
    </div>
    <div class="col-8">
                <table class="table">
            <thead>
                <tr>
                <th scope="col">اسم المتجر</th>
                <th scope="col">رقم الطلب</th>
                <th scope="col">الدفع</th>
                <th scope="col">الحالة</th>
                <th scope="col">رقم السلعة</th>
                <th scope="col">إجراءات</th>
                </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
            <tr>
                <td>{{ $order -> store->name }}</td>
                <td>{{ $order -> transaction_id }}</td>
                <td>{{ $order -> payment_type }}</td>
                <td>{{ $order -> status }}</td>
                <td>{{ $order-> id }}</td>
                <td>
                    <a href="/orders/{{ $order->id }}" style="width:100%;" class="btn btn-dark">  رؤية التفاصيل<i class="fa fa-search"></i></a>

                </td>
            </tr>
            @endforeach
            </table>
            </tbody>
    </div>
</div>

@endsection