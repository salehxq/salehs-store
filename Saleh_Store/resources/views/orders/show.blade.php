@extends('layouts.app')

@section('content')
 <div class="row mb-4">
     @if(auth()->user()->id == $order->store->user_id)
        <a class="btn btn-dark" href="/stores/orders">العودة الى الطلبات</a>
    @else
        <a class="btn btn-dark" href="/orders">العودة الى الطلبات</a>
    @endif
 </div>
<div class="row">
    <div class="col-4">
    <div class="card" style="width: 100%;">
  <ul class="list-group list-group-flush">
    <li class="list-group-item">
    <b>اسم المتسوق</b>
    <p>{{ $order->user->name }}</p>
    </li>
    <li class="list-group-item">
    <b>رقم الهاتف</b>
    <p>{{ $order->user->number }}</p>
    </li>
    <li class="list-group-item">
    <b>البريد الإلكتروني</b>
    <p>{{ $order->user->email }}</p>
    </li>

  </ul>
</div>
</div>
    <div class="col-8">
                <table class="table">
            <thead>
                <tr>
                <th scope="col">السلعة</th>
                <th scope="col">السعر</th>
                <th scope="col">الكمية</th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ $order->products }}</td>
                <td>{{ $order->products }}</td>
                <td>{{ $order->products }}</td>
            </tr>
            <tr>
                <td>
                    المجموع:
                </td>
            </tr>
            </table>
            </tbody>
    </div>
</div>

@endsection