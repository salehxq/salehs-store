@extends('layouts.app')

@section('content')
<style>
    .coloring:hover{
        box-shadow: 10px 10px 0px #98AD06 ;
        top: -5px;
        left: -5px;
    }
    .coloring{
        background:#344D2A;
        color:#FAFAFE
    }
    .coloring1:hover{
        box-shadow: 10px 10px 0px #344D2A ;
        top: -5px;
        left: -5px;
    }
    .coloring1{
        background:#98AD06;
        color:#FAFAFE
    }
</style>
<center>
<i class="fas fa-calendar-check fa-5x"></i>
<br><br>
<h5>تمت عملية الشراء بنجاح</h5>
<a href="/" class="btn coloring" style="width:100%">العودة إلى المتجر</a>
<br><br>
<a href="https://www.google.com/" class="btn coloring1" style="width:100%">الذهاب الى المتصفح</a>
</center>
<?php
session()->forget('currentOrders');
?>
@endsection