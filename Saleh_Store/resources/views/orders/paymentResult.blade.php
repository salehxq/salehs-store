@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-12">
        @if($status == 'CAPTURED')
        تم استلام طلبيتك وسنتواصل معك قريباً
        @else
        تعذر لم نتمكن من اكمال الطلب حاول <a href="/orders/create">مجدداً</a>
        @endif
    </div>
</div>
@endsection