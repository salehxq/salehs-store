@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-4">
    @component('shared.accountsList',['createstore'=>'active'])
    @endcomponent
    </div>
    <div class="col-4">
    @include('shared.errors')
    <form method="post" action="/products/{{ $product->id }}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="mb-3">
        <label for="name" class="form-label">أسم السلعة</label>
        <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"  value="{{ $product->name }}" placeholder="أسم السلعة" required>
    </div>

    <div class="mb-3">
        <label for="price" class="form-label">سعر السلعة</label>
        <input type="text" name="price" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $product->price }}" placeholder="سعر السلعة" required>
    </div>

    <div class="mb-3">
        <label for="logo" class="form-label">صورة السلعة</label>
        <input name="image1" class="form-control form-control-sm" id="image1" type="file">
        <input name="image2" class="form-control form-control-sm" id="image2" type="file">
    </div>
    <button type="submit" class="btn btn-dark">تعديل</button>
    </form>
</div>
@endsection
    </div>
</div>
