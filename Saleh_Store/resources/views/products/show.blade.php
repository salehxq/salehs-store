@extends('layouts.app')

@section('styles')
<style>
    .productImage img{
        width: 100%;
        max-height: 400px;
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <a href="/stores/{{ $product->store->id }}">@lang('messages.Return') {{ $product->store->name }}</a>
    </div>
</div>
<div class="row">
    <div class="col-4 productImage">
        @isset($product->images)
        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
    @foreach( $product->images as $image)
    <div class="carousel-item {{ $loop->first ? 'active' : ''}} ">
        <img src="{{ $image }}" class="d-block w-100" alt="...">
            </div>
    @endforeach
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
        </div>
        @endisset
        @empty($product->images)
            <img src="{{ asset('programmer1.gif') }}">
        @endempty
    </div>
    <div class="col-8">
    @include('shared.messages')
    <p><b>@lang('messages.productname')</p></b>
    <p class="mt-2">{{ $product->name }}</p>
    <p><b>@lang('messages.productprice')</p></b>
    <p class="mt-2">{{ $product->price }}</p>
    <br>
    @auth
    <form method="POST" action="/orders/addProduct/{{ $product->id }}">
        @csrf
    <label for="exampleFormControlSelect1">@lang('messages.Quantity')</label>
        <select name="quantity" class="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
            <option>9</option>
            <option>10</option>
            <option>11</option>
            <option>12</option>
            <option>13</option>
            <option>14</option>
            <option>15</option>
            <option>16</option>
            <option>17</option>
            <option>18</option>
            <option>19</option>
            <option>20</option>
        </select>
        <button type="submit" class="btn mt-2" style="background:#FCA310">@lang('messages.AddProduct')</button>
    </form>
    @endauth
    @guest
    <b><p style="color:#A20A03">يجب عليك <a href="/users/login">تسجيل الدخول</a> قبل الشراء</p></b>
    @endguest
</div>
</div>
@endsection