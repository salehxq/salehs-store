@extends('layouts.app')

@section('content')
@section('styles')
<style>
    .card{
        min-width:200px;
        max-width:200px;
        margin: 2% !important;
    }
    .card img{
        height:150px;
        object-fit:cover;
    }
</style>
@endsection

<div class="row">
    <div class="col-4">
    @component('shared.accountsList',['stores'=>'active'])
    @endcomponent
    </div>
    <div class="col-8">
    <a href="/products/create/{{ $store->id }}" class="btn btn-dark">إضافة سلعة</a>
    <div class="card-group">
            @foreach( $store->products as $product)
                <div class="card" style="width: 60%;">
            
                @isset($product->images[0])
                    <img src="{{ asset($product->images[0]) }}" class="card-img-top" alt="...">
                @endisset

                @empty($product->images[0])
                    <img src="{{ asset('product.jpg') }}" class="card-img-top" alt="...">
                @endempty
                <div class="card-body">   
                    <h5 class="card-title">{{ $product -> name }}</h5>
                    <p class="card-text"> سعر السلعة: {{ $product -> price }} درهم</p>
            <center>
                    <a href="/products/{{ $product -> id }}/edit" class="btn btn-dark">تعديل</a>
                    <br><br>
                    <a href="/products/{{ $product -> id }}/delete" class="btn btn-danger">مسح السلعة</a>
            </center>
                </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
</div>
    </div>
</div>
@endsection
