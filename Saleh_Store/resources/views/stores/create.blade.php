@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-4">
    @component('shared.accountsList',['createstore'=>'active'])
    @endcomponent
    </div>
    <div class="col-4">
    @include('shared.errors')
    <form method="post" action="/stores/store" enctype="multipart/form-data">
    @csrf
    <div class="mb-3">
        <label for="name" class="form-label">أسم المتجر</label>
        <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="أسم المتجر الخاص بك" required>
    </div>

    <div class="mb-3">
        <label for="description" class="form-label">وصف المتجر</label>
        <input type="text" name="description" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="وصف المتجر الخاص بك" required>
    </div>

    <div class="mb-3">
        <label for="logo" class="form-label">صورة المتجر</label>
        <input name="logo" class="form-control form-control-sm" id="formFileSm" type="file">
    </div>
    <button type="submit" class="btn btn-dark">إنشاء</button>
    </form>
</div>
@endsection
    </div>
</div>
