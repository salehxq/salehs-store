@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-4">
    @component('shared.accountsList',['StoreOrders'=>'active'])
    @endcomponent
    </div>
    <div class="col-8">
                <table class="table">
            <thead>
                <tr>
                <th scope="col">اسم المتجر</th>
                <th scope="col">رقم الطلب</th>
                <th scope="col">الدفع</th>
                <th scope="col">الحالة</th>
                <th scope="col">السلع</th>
                <th scope="col">إجراءات</th>
                </tr>
            </thead>
            <tbody>
            @foreach($orders as $order1)
            <tr>
                <td>{{ $order1 -> store->name }}</td>
                <td>{{ $order1 -> transaction_id }}</td>
                <td>{{ $order1 -> payment_type }}</td>
                <td>{{ $order1 -> status }}</td>
                <td>
                    <a href="/orders/{{ $order1->id }}" style="width:100%;" class="btn btn-dark">  رؤية التفاصيل<i class="fa fa-search"></i></a>
                    @if(in_array($order1->status, ['paid','pending']))
                    <br><br>
                    <a href="/orders/{{ $order1->id }}/deliverd" style="width:100%;" class="btn btn-dark">تم التوصيل<i class="fa fa-car"></i></a>
                    @endif
                </td>
            </tr>
            @endforeach
            </table>
            </tbody>

    </div>
</div>

@endsection