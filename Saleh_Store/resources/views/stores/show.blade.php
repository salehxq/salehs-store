@extends('layouts.app')

@section('content')

@section('styles')
<style>
    .card{
        min-width:200px;
        max-width:200px;
        margin: 2% !important;
    }
    .card img{
        height:150px;
        object-fit:cover;
    }
    .border:hover{
        background-color:#FCC729;
        color:white
    }
    .btn-my{
        background-color:#FCA310
    }
</style>
@endsection
<center>
<div class="row">
    <div class="col-12">
            <div class="card-group">
                    @foreach($store ->products as $product)
                    <div class="card" style="width: 60%;">
            
            @isset($product->images[0])
                <img src="{{ asset($product->images[0]) }}" class="card-img-top" alt="...">
            @endisset

            @empty($product->images[0])
                <img src="{{ asset('programmer2.gif') }}" class="card-img-top" alt="...">
            @endempty
            <div class="card-body">   
                <h5 class="card-title">@lang('messages.productname')<br>{{$product -> name }} </h5>
                <p class="card-text"> @lang('messages.productprice') {{ $product -> price }} @lang('messages.AED')</p>
        <center>
                <a href="/products/{{ $product -> id }}" style="100%" class="btn btn-my">@lang('messages.productvisit')</a>
                <br><br>
                @auth
                <form method="POST" action="/orders/addProduct/{{ $product->id }}">
                @csrf
                <select style="width:100%" name="quantity" class="form-select form-select-sm mb-3" aria-label=".form-select-lg example">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
            <option>9</option>
            <option>10</option>
            <option>11</option>
            <option>12</option>
            <option>13</option>
            <option>14</option>
            <option>15</option>
            <option>16</option>
            <option>17</option>
            <option>18</option>
            <option>19</option>
            <option>20</option>
        </select>
                <button type="submit" class="btn border rounded-circle border-3 mt-2"><i class="fas fa-cart-plus fa-1x"></i></button>
                </form>
                @endauth
        </center>
            </div>
            </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</center>
@endsection