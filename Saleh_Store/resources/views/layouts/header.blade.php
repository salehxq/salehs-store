<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">@lang('messages.Store')</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        @guest
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="/users/login">@lang('messages.login')</a>
        </li>
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <div class="dropdown">
        <button class="btn btn-sm btn-dark dropdown-toggle" type="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
          @lang('messages.chooselang')    
      </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
          <li><a class="dropdown-item" href="/ar">العربية</a></li>
          <li><a class="dropdown-item" href="/en">English</a></li>
        </ul>
      </div>
        @endguest
        @auth
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <div class="dropdown">
        <button class="btn btn-sm btn-dark dropdown-toggle" type="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
          @lang('messages.chooselang')    
      </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
          <li><a class="dropdown-item" href="/ar">العربية</a></li>
          <li><a class="dropdown-item" href="/en">English</a></li>
        </ul>
      </div>
        <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="/">@lang('messages.Home')</a>
        </li>
        <li class="nav-item">
        @if(session()->has('currentOrders'))
        @foreach(session('currentOrders') as $product)
        @endforeach
        @endif
        @if(session()->has('currentOrders') && $product -> quantity < 9)
        <a class="nav-link active" aria-current="" href="/orders/create">{{ $product -> quantity }}<i class="fas fa-shopping-cart"></i>@lang('messages.Cart') </a>
        @elseif(session()->has('currentOrders') && $product -> quantity == 9)
        <a class="nav-link active" aria-current="" href="/orders/create">{{ $product -> quantity }}<i class="fas fa-shopping-cart"></i>@lang('messages.Cart') </a>
        @elseif(session()->has('currentOrders') && $product -> quantity > 9)
        <a class="nav-link active" aria-current="" href="/orders/create">+9<i class="fas fa-shopping-cart"></i>@lang('messages.Cart') </a>
        @endif
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/users/logout">@lang('messages.Logout')</a>
        </li>
        <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                @lang('messages.More')
                </a>
                <ul style="text-align:right" class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><a class="dropdown-item" href="/users/edit">@lang('messages.Profile')</a></li>
                  <li><a class="dropdown-item" href="/orders">@lang('messages.MyOrders')</a></li>
                  <li><a class="dropdown-item" href="#">@lang('messages.Contact')</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        @endauth
</nav>