<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\Store;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->text($maxNbChars = 10),
            'price' =>  $this->faker->randomFloat($nbMaxDecimals = 3, $min = 1, $max = 100),
            'store_id' => Store::all()->random()->id,
    ];;
    }
}
