<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\Store;

use App\Models\User;

use App\Models\Product;

function generateProducts(){

    $products = array();
    for ($i = 0; $i < rand(1,3); $i++){
        $product = Product::all()->random();
        $product ->quantity=rand(1,4);
        $products[]=$product;
    }
    return json_encode($products);

   }

class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */


    public function definition()
    {
        return [
            'transaction_id' => $this->faker->randomNumber($nbDigits = 8, $strict = false),
            'payment_type' =>  $this->faker->randomElement($array = array ('visa','knet','cash')),
            'status' =>  $this->faker->randomElement($array = array ('delivered','paid','pending','failed')),
            'products' => generateProducts(),
            'store_id' => Store::all()->random()->id,
            'user_id' => User::all()->random()->id,
    ];;
    }
}
